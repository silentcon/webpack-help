const webpack = require('webpack')
const path = require('path')

module.exports = {
  entry: {
    app: [ 'webpack/hot/dev-server', 'webpack-dev-server/client?http://localhost:8000', './src/main.js']
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: path.resolve(__dirname, 'build'),
    filename: 'bundle.js'
  },
  module: {
    preloaders: [
      {
        test:/\.vue$/,
        loader: 'eslint',
        include: path.join(__dirname + '/src')
      },
      {
        test: /\.js$/,
        loader: 'eslint',
        include: path.join(__dirname + '/src')
      }
    ],
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        loader: 'babel',
        include: path.join(__dirname + '/src')
      },
    ]
  },
  vue: {
    postcss: [
      require('postcss-cssnext')
    ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
}
