const express = require('express')
const app = express()

const webpack = require('webpack')
const webpackDevServer = require('webpack-dev-server')
const webpackConfig = require('./webpack.config')
const compiler = webpack(webpackConfig)

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true, publicPath: webpackConfig.output.publicPath, stats: { colors: true }
}))

app.use(require('webpack-hot-middleware')(compiler))

const bundler = new webpackDevServer(compiler, {
  publicPath: '/build/', hot: true, quiet: false, stats: { colors: true }
})

bundler.listen(8000, 'localhost', () => {
    console.log('listening');
})
